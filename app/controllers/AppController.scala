package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class AppController @Inject() extends Controller {

  def index = Action.async(Future(Ok(views.html.app.index())))

}
